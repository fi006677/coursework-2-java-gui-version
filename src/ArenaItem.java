package uk.ac.reading.fi006677.javafx;


/**
 * Abstract class for all objects in the arena
 */
public abstract class ArenaItem {
	protected double x, y, rad;			// position, size, angle of movement and speed   
										// protected not private for use of inheritance later
	static int droneCounter = 0;		// used to give each drone a unique identifier
	protected int droneID;				// unique identifier for drone
	/**
	 * 
	 */
	public ArenaItem() {
		this(50, 50, 25);
	}
	
	/**
	 * construct arena item
	 * @param ix	initial x position
	 * @param iy	y position
	 * @param ir	radius
	 */
	public ArenaItem(double ix, double iy, double ir) {
		x = ix;
		y = iy;
		rad = ir;
		droneID = droneCounter++;	
	}
	
	/**
	 * return x position
	 * @return
	 */
	public double getX() { return x; }
	
	/**
	 * return y position
	 * @return
	 */
	public double getY() { return y; }
	
	/**
	 * return radius of drone
	 * @return
	 */
	public double getRad() { return rad; }
	
	/** 
	 * set the drone at position nx,ny
	 * @param nx
	 * @param ny
	 */
	public void setXY(double nx, double ny) {
		x = nx;
		y = ny;
	}
	
	/**
	 * return the identity of drone
	 * @return
	 */
	public int getID() {return droneID; }
	
	/**
	 * draw a drone into the interface
	 */
	public void drawDrone(GUICanvas mc) {
		mc.showDrone(x, y, rad);
	}
	
	protected String getStrType() {
		return "ArenaItem";
	}
	
	
	
	/** 
	 * return string describing drone
	 */
	public String toString() {
		return getStrType()+" at "+Math.round(x)+", "+Math.round(y);
	}
		
	/**
	 * abstract method for checking a drone in arena b
	 * @param b
	 */
	protected abstract void checkDrone(DroneArena d);
	
	/**
	 * abstract method for adjusting a drone
	 */
	protected abstract void adjustDrone();
	
	/**
	 * is drone at ox,oy size or hitting this drone
	 * @param ox
	 * @param oy
	 * @param or
	 * @return true if hitting
	 */
	public boolean touching(double ox, double oy, double or) {
		return (ox-x)*(ox-x) + (oy-y)*(oy-y) < (or+(2*rad))*(or+(2*rad));
	}		// touching if distance between drone and ox,oy < ist rad + or
	
	/** is drone touching the other drone
	 * 
	 * @param oDrone - the other drone
	 * @return true if touching
	 */
	public boolean touching (ArenaItem oDrone) {
		return touching(oDrone.getX(), oDrone.getY(), oDrone.getRad());
	}

}
