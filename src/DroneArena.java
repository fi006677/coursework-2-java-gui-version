package uk.ac.reading.fi006677.javafx;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;
import java.util.Random;

import javafx.scene.paint.Color;

/**
 * Class to hold one or more drones, of type Drone initially
 */
public class DroneArena {
	double xSize, ySize;								// size of arena
	double posX, posY;									// positions of new drones
	private ArrayList<ArenaItem> ManyDrones;			// array list of all drones in arena
	
	/**
	 * construct an arena
	 */
	DroneArena() {
		this(900, 500);			// default size
	}
	
	/**
	 * construct arena
	 * @param xS	x value for the arena size
	 * @param yS	y value for the arena size
	 */
	DroneArena(double xS, double yS){
		xSize = xS;
		ySize = yS;
		ManyDrones = new ArrayList<ArenaItem>();					// list of all drones, initially empty
		ManyDrones.add(new LowDrone(xSize/3, ySize/2, 10, 45, 6));	// add low drone
		ManyDrones.add(new HighDrone(xSize/2, ySize/3, 15, 0, 4));	// add high drone
		ManyDrones.add(new Obstacle(xSize/2, ySize/2, 25));			// add obstacle
	}
	
	/**
	 * return arena size in x direction
	 * @return
	 */
	public double getXSize() {
		return xSize;
	}
	/**
	 * return arena size in y direction
	 * @return
	 */
	public double getYSize() {
		return ySize;
	}
	
	/**
	 * draw all drones in the arena into the interface
	 */
	public void drawArena(GUICanvas gc) {
		for (ArenaItem d : ManyDrones) {						// for all drones
	 		if (d instanceof LowDrone) {
	 			gc.setFillColour(Color.BLUE);
	 			d.drawDrone(gc);
	 		}
	 		else if (d instanceof HighDrone) {
	 			gc.setFillColour(Color.GREEN);
	 			d.drawDrone(gc);
	 		}
	 		else if (d instanceof Obstacle) {
	 			gc.setFillColour(Color.RED);				// set appropriate fill colour
	 			d.drawDrone(gc);							// then draw
	 		}
	 	}		// draw all drones
	}
	
	/**
	 * check all drones .. see if need to change angle of moving drones, etc 
	 */
	public void checkDrones() {
		for (ArenaItem ai : ManyDrones) ai.checkDrone(this);	// check all drones
	}
	
	/**
	 * adjust all drones .. move any moving ones
	 */
	public void adjustDrones() {
		for (ArenaItem ai : ManyDrones) ai.adjustDrone();
	}
	 	
	/**
	 * return list of strings defining each drone
	 * @return
	 */
	public ArrayList<String> describeAll() {
		ArrayList<String> ans = new ArrayList<String>();		// set up empty arraylist
		for (ArenaItem ai : ManyDrones) ans.add(ai.toString());			// add string defining each drones
		return ans;												// return string list
	}
	
	/** 
	 * Check angle of drone ... if hitting wall, rebound; if hitting drone, change angle
	 * @param x				x position
	 * @param y				y position
	 * @param rad			radius of drone
	 * @param ang			current angle
	 * @param notID			identify of drone to be ignored
	 * @return				new angle 
	 */
	public double CheckDroneAngle(double x, double y, double rad, double ang, int notID) {
		double ans = ang;
		if (x < rad || x > xSize - rad) ans = 180 - ans;
			// if drone collided with wall on left or right side, set mirror angle, being 180-angle
		if (y < rad || y > ySize - rad) ans = - ans;
			// if try to go off top or bottom, set mirror angle
		
		for (ArenaItem ai : ManyDrones) 
			if (ai.getID() != notID && ai.touching(x, y, rad)) ans = 180*Math.atan2(y-ai.getY(), x-ai.getX())/Math.PI;
				// check all drones except one with given id
				// if hitting, return angle between the other drone and this one.
		
		return ans;		// return the angle
	}

	/**
	 * check if the drone has been hit by another drone
	 * @param drone		the other drone
	 * @return 	true if hit
	 */
	public boolean checkHit(ArenaItem drone) {
		boolean ans = false;
		for (ArenaItem ai : ManyDrones)
			if (ai instanceof Obstacle && ai.touching(drone)) ans = true;
				// try all drone check if hitting another drone
		return ans;
	}
	
	/**
	 * adds a new low flying drone to the arraylist using the random generator
	 */
	public void addLowDrone() {
		Random randomGenerator;
		randomGenerator = new Random();	
		posX = randomGenerator.nextInt((int) (xSize-1)) + 1;	// assigns a random x value
		posY = randomGenerator.nextInt((int) (ySize-1)) + 1;	// assigns a random y value
		ManyDrones.add(new LowDrone(posX, posY, 10, 45, 6));	// adds a new low drone with the assigned values to the arraylist
	}
	
	/**
	 * adds a new high flying drone to the arraylist using the random generator
	 */
	public void addHighDrone() {
		Random randomGenerator;
		randomGenerator = new Random();	
		posX = randomGenerator.nextInt((int) (xSize-1)) + 1;	// assigns a random x value
		posY = randomGenerator.nextInt((int) (ySize-1)) + 1;	// assigns a random y value
		ManyDrones.add(new HighDrone(posX, posY, 15, 0, 4));	// adds a new low drone with the assigned values to the arraylist
	}
	
	/**
	 * adds a new obstacle to the arraylist using the random generator
	 */
	public void addObstacle() {
		Random randomGenerator;
		randomGenerator = new Random();	
		posX = randomGenerator.nextInt((int) (xSize-1)) + 1;	// assigns a random x value
		posY = randomGenerator.nextInt((int) (ySize-1)) + 1;	// assigns a random y value
		ManyDrones.add(new Obstacle(posX, posY, 25));			// adds a new obstacle with the assigned values to the arraylist
	}
	
	/**
	 * resets the arena for new drones to be added
	 */
	public void resetArena() {
		ManyDrones.clear();		// clears the arena
	}
	
	/**
	 * allows the user to save the arena to a .txt file
	 * @param name	the name of the file to be saved
	 */
	public void saveArena(Optional<String> name) throws FileNotFoundException {
		try {
  	      File saveArena = new File(name + ".txt"); 							//creates a new file
  	      if (saveArena.createNewFile()) {
  	    	  FileWriter myWriter = new FileWriter(saveArena);
  	    	for (ArenaItem ai : ManyDrones) myWriter.write(ai.toString());		// prints the information of every drone
  	    	myWriter.close();													//closes the file
  	      }
  	    } catch (IOException e) {  	      
  	      e.printStackTrace();
  	    }
	}

}