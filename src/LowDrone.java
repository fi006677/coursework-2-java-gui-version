/**
 * 
 */
package uk.ac.reading.fi006677.javafx;

/**
 * Class for a low flying drone
 * Initially moves diagonally across the arena and changes direction if it comes into contact with another drone or an obstacle.
 */
public class LowDrone extends Drone {

	double angle, speed;			// angle and speed of travel
	/**
	 * 
	 */
	public LowDrone() {
		// TODO Auto-generated constructor stub
	}

	/** Create low flying drone, size ir ay ix,iy, moving at angle ia and speed is
	 * @param ix
	 * @param iy
	 * @param ir
	 * @param ia
	 * @param is
	 */
	public LowDrone(double ix, double iy, double ir, double ia, double is) {
		super(ix, iy, ir, ia, is);
		angle = ia;
		speed = is;
	}

	/**
	 * checkDrone - change angle of travel if hitting wall or another drone
	 * @param da   droneArena
	 */
	@Override
	protected void checkDrone(DroneArena da) {
		angle = da.CheckDroneAngle(x, y, rad, angle, droneID);
	}

	/**
	 * adjustDrone
	 * Here, move drone depending on speed and angle
	 */
	@Override
	protected void adjustDrone() {
		double radAngle = angle*Math.PI/180;		// put angle in radians
		x += speed * Math.cos(radAngle);		// new X position
		y += speed * Math.sin(radAngle);		// new Y position
	}
	/**
	 * return string defining drone type
	 */
	protected String getStrType() {
		return "Low flying drone";
	}

}
