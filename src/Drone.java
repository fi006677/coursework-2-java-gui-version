package uk.ac.reading.fi006677.javafx;

/**
 * Abstract class for simple drone which can move around an area
 */
public abstract class Drone extends ArenaItem {
	double angle, speed;			// angle and speed of travel
	
	/**
	 * 
	 */
	public Drone() {
	}

	/** Create drone, size ir ay ix,iy, moving at angle ia and speed is
	 * @param ix
	 * @param iy
	 * @param ir
	 * @param ia
	 * @param is
	 */
	public Drone(double ix, double iy, double ir, double ia, double is) {
		super(ix, iy, ir);
		angle = ia;
		speed = is;
	}
	
	/** 
	 * return string describing drone
	 */
	public String toString() {
		return getStrType()+" at "+Math.round(x)+", "+Math.round(y) + ", moving at speed " + Math.round(speed) +   
				" \n and travelling at angle " + Math.round(angle);
	}

	/**
	 * checkDrone - change angle of travel if hitting wall or another drone
	 * @param da   droneArena
	 */
	@Override
	protected void checkDrone(DroneArena da) {
		angle = da.CheckDroneAngle(x, y, rad, angle, droneID);
	}

	/**
	 * adjustDrone
	 * Here, move drone depending on speed and angle
	 */
	@Override
	protected void adjustDrone() {
		double radAngle = angle*Math.PI/180;		// put angle in radians
		x += speed * Math.cos(radAngle);		// new X position
		y += speed * Math.sin(radAngle);		// new Y position
	}
	/**
	 * return string defining drone type
	 */
	protected String getStrType() {
		return "Drone";
	}

}
	


