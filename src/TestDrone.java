package uk.ac.reading.fi006677.javafx;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Optional;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * The interface that the main application uses
 */
public class TestDrone extends Application {
	private GUICanvas gc;
	private AnimationTimer timer;								// timer used for animation
	private VBox rtPane;										// vertical box for putting info
	private DroneArena arena;

	/**
	 * function to show in a box the information about the simulation
	 */
	private void showAbout() {
	    Alert alert = new Alert(AlertType.INFORMATION);				// define what box is
	    alert.setTitle("About");									// say is About
	    alert.setHeaderText(null);
	    alert.setContentText("GUI Drone Simulation by Kat Huxham");			// give text
	    alert.showAndWait();										// show box and wait for user to close
	}
	
	/**
	 * function to show in a box the saving function that the user can use
	 */
	private Optional<String> saveWorld() {
		TextInputDialog dialog = new TextInputDialog("arena");		// generates a default name
		dialog.setTitle("Save your arena");							// says Save your arena
		dialog.setContentText("Please enter a file name:");			// prompts the user to choose a file name to save their arena as
		return dialog.showAndWait();								// returns the user's input
	}
	
	

	 /**
	  * set up the mouse event - when mouse pressed, put drone there
	  * @param canvas
	  */
	void setMouseEvents (Canvas canvas) {
	       canvas.addEventHandler(MouseEvent.MOUSE_PRESSED, 		// for MOUSE PRESSED event
	    	       new EventHandler<MouseEvent>() {
	    	           @Override
	    	           public void handle(MouseEvent e) {
	  		            	drawWorld();							// redraw world
	  		            	drawStatus();
	    	           }
	    	       });
	}
	/**
	 * set up the menu of commands for the GUI
	 * @return the menu bar
	 */
	MenuBar setMenu() {
		MenuBar menuBar = new MenuBar();						// create main menu
	
		Menu mFile = new Menu("File");							// add File main menu
		MenuItem mSave = new MenuItem("Save");					// let user save their arena
		mSave.setOnAction(new EventHandler<ActionEvent>() {
		    public void handle(ActionEvent t) {
		    	try {
					arena.saveArena(saveWorld());				// calls the function to save the arena
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}											 	// saves the arena
		    }
		});
		MenuItem mExit = new MenuItem("Exit");					// whose sub menu has Exit
		mExit.setOnAction(new EventHandler<ActionEvent>() {
		    public void handle(ActionEvent t) {					// action on exit is
	        	timer.stop();									// stop timer
		        System.exit(0);									// exit program
		    }
		});
		mFile.getItems().addAll(mSave, mExit);					// add exit to File menu
		
		Menu mHelp = new Menu("Help");							// create Help menu
		MenuItem mAbout = new MenuItem("About");				// add About sub men item
		mAbout.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
            	showAbout();									// and its action to print about
            }	
		});
		mHelp.getItems().addAll(mAbout);						// add About to Help main item
		
		menuBar.getMenus().addAll(mFile, mHelp);				// set main menu with File, Help
		return menuBar;											// return the menu
	}

	/**
	 * set up the horizontal box for the bottom with relevant buttons
	 * @return
	 */
	private HBox setButtons() {
	    Button btnStart = new Button("Start");					// create button for starting
	    btnStart.setOnAction(new EventHandler<ActionEvent>() {	// now define event when it is pressed
	        @Override
	        public void handle(ActionEvent event) {
	        	timer.start();									// its action is to start the timer
	       }
	    });

	    Button btnStop = new Button("Pause");					// now button for stop
	    btnStop.setOnAction(new EventHandler<ActionEvent>() {
	        @Override
	        public void handle(ActionEvent event) {
	           	timer.stop();									// and its action to stop the timer
	       }
	    });
	    
	    Button btnReset = new Button("Reset");							// button to reset
	    btnReset.setOnAction(new EventHandler<ActionEvent>() {
	        @Override
	        public void handle(ActionEvent event) {
	        	timer.start();
	        	arena.resetArena();										// resets the arena
	       }
	    });
	    

	    Button btnAddLow = new Button("Low flying drone");				// button to add another low flying drone
	    btnAddLow.setStyle("-fx-background-color: #89C8FF; ");
	    btnAddLow.setOnAction(new EventHandler<ActionEvent>() {
	        @Override
	        public void handle(ActionEvent event) {
	           	arena.addLowDrone();									// add the drone
	           	drawWorld();											// redraws the arena to add it in
	       }
	    });
	    
	    Button btnAddHigh = new Button("High flying drone");			// button to add another high flying drone
	    btnAddHigh.setStyle("-fx-background-color: #89CE75; ");
	    btnAddHigh.setOnAction(new EventHandler<ActionEvent>() {
	        @Override
	        public void handle(ActionEvent event) {
	           	arena.addHighDrone();									// add the drone
	           	drawWorld();											// redraws the arena to add it in
	       }
	    });
	    
	    Button btnAddObs = new Button("Obstacle");						// button to add another obstacle
	    btnAddObs.setStyle("-fx-background-color: #FF8483; ");
	    btnAddObs.setOnAction(new EventHandler<ActionEvent>() {
	        @Override
	        public void handle(ActionEvent event) {
	           	arena.addObstacle();									// add the drone
	           	drawWorld();											// redraws the arena to add it in
	       }
	    });
	    
	    // now add these buttons + labels to a HBox
	    return new HBox(new Label("Run: "), btnStart, btnStop, btnReset, new Label("          Add: "), btnAddLow, btnAddHigh, btnAddObs);
	}

	
	/** 
	 * draw the world with drone in it
	 */
	public void drawWorld () {
	 	gc.clearCanvas();						// set beige colour
	 	arena.drawArena(gc);
	}
	
	/**
	 * show where drone is, in pane on right
	 */
	public void drawStatus() {
		rtPane.getChildren().clear();					// clear rtpane
		ArrayList<String> allBs = arena.describeAll();
		for (String s : allBs) {
			Label l = new Label(s); 		// turn description into a label
			rtPane.getChildren().add(l);	// add label	
		}	
	}


	/**
	 * loads the stage for the application
	 */
	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub
		primaryStage.setTitle("GUI Drone Simulation");
	    BorderPane bp = new BorderPane();
	    bp.setPadding(new Insets(10, 20, 10, 20));

	    bp.setTop(setMenu());											// put menu at the top
  	    
	    Group root = new Group();										// create group with canvas
	    Canvas canvas = new Canvas( 800, 500 );
	    root.getChildren().add( canvas );
	    bp.setLeft(root);												// load canvas to left area
	
	    gc = new GUICanvas(canvas.getGraphicsContext2D(), 800, 500);

	    setMouseEvents(canvas);											// set up mouse events

	    arena = new DroneArena(800, 500);								// set up arena
	    drawWorld();
	    
	    timer = new AnimationTimer() {									// set up timer
	        public void handle(long currentNanoTime) {					// and its action when on
	        		arena.checkDrones();									// check the angle of all drones
		            arena.adjustDrones();								// move all drones
		            drawWorld();										// redraw the world
		            drawStatus();										// indicate where drones are
	        }
	    };

	    rtPane = new VBox();											// set vBox on right to list items
		rtPane.setAlignment(Pos.TOP_LEFT);								// set alignment
		rtPane.setPadding(new Insets(5, 75, 75, 5));					// padding
 		bp.setRight(rtPane);											// add rtPane to borderpane right
 		
 		
	    bp.setBottom(setButtons());										// set bottom pane with buttons

	    Scene scene = new Scene(bp, 1100, 600);							// set overall scene
        bp.prefHeightProperty().bind(scene.heightProperty());
        bp.prefWidthProperty().bind(scene.widthProperty());

        primaryStage.setScene(scene);
        primaryStage.show();
	  

	}

	/**
	 * loads the interface
	 * @param args
	 */
	public static void main(String[] args) {
	    Application.launch(args);			// launch the GUI

	}

}