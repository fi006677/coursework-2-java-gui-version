package uk.ac.reading.fi006677.javafx;

/**
 * Class for a high flying drone
 * Moves horizontally across the arena and deflects low flying drones 
 * until it comes into contact with another high flying drone or an obstacle
 */
public class HighDrone extends Drone {
	
	double angle, speed;			// angle and speed of travel
	/**
	 * 
	 */
	public HighDrone() {
		// TODO Auto-generated constructor stub
	}

	/** Create high drone, size ir ay ix,iy, moving at angle ia and speed is
	 * @param ix
	 * @param iy
	 * @param ir
	 * @param ia
	 * @param is
	 */
	public HighDrone(double ix, double iy, double ir, double ia, double is) {
		super(ix, iy, ir, ia, is);
		angle = ia;
		speed = is;
	}

	/**
	 * checkDrone - change angle of travel if hitting wall or another drone
	 * @param da   droneArena
	 */
	@Override
	protected void checkDrone(DroneArena da) {
		angle = da.CheckDroneAngle(x, y, rad, angle, droneID);
	}

	/**
	 * adjustDrone
	 * Here, move drone depending on speed and angle
	 */
	@Override
	protected void adjustDrone() {
		double radAngle = angle*Math.PI/180;		// put angle in radians
		x += speed * Math.cos(radAngle);		// new X position
		y += speed * Math.sin(radAngle);		// new Y position
	}
	/**
	 * return string defining drone type
	 */
	protected String getStrType() {
		return "High flying drone";
	}

}
