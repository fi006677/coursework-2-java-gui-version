/**
 * 
 */
package uk.ac.reading.fi006677.javafx;


/**
 * Class for an obstacle which does not move but blocks the movement of the drones
 */
public class Obstacle extends ArenaItem{
	double angle, speed;			// angle and speed of travel
	/**
	 * 
	 */
	public Obstacle() {
		
		// TODO Auto-generated constructor stub
	}

	/** Create game ball, size ir ay ix,iy, moving at angle ia and speed is
	 * @param ix
	 * @param iy
	 * @param ir
	 */
	public Obstacle(double ix, double iy, double ir) {
		super(ix, iy, ir);
	}

	/**
	 * checkDrone - change angle of travel if hitting wall or another drone
	 * @param da   droneArena
	 */
	@Override
	protected void checkDrone(DroneArena da) {
		angle = da.CheckDroneAngle(x, y, rad, angle, droneID);
	}

	/**
	 * adjustDrone
	 * Here, move drone depending on speed and angle
	 */
	@Override
	protected void adjustDrone() {
		double radAngle = angle*Math.PI/180;		// put angle in radians
		x += speed * Math.cos(radAngle);		// new X position
		y += speed * Math.sin(radAngle);		// new Y position
	}
	/**
	 * return string defining ball type
	 */
	protected String getStrType() {
		return "Obstacle";
	}


}
